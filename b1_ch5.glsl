/**
 * b1_ch5.glsl
 * Ray Tracing in One Weekend
 * Chapter 5 - Adding a Sphere
 *
 * NOTE: Not accounting for aspect yet, so "sphere" may be rendered to look like an ellipse depending on resolution.
 */

 #include "./common.glsl"

vec3 color(Ray r)
{
    if(hitSphere(vec3(0.0, 0.0, -1.0), 0.5, r) > 0.0)
    {
        return vec3(1.0, 0.0, 0.0);
    }
    float t = 0.5 * (r.d.y + 1.0);
    return (1.0 - t) * vec3(1.0) + t * vec3(0.5, 0.7, 1.0);
}

void main()
{
    vec3 lowerLeftCorner = vec3(-2.0, -1.0, -1.0);
    vec3 horizontal = vec3(4.0, 0.0, 0.0);
    vec3 vertical = vec3(0.0, 2.0, 0.0);
    vec3 origin = vec3(0.0, 0.0, 0.0);
    vec2 uv = gl_FragCoord.xy / iResolution.xy;
    Ray r = createRay(
        origin,
        normalize(lowerLeftCorner + uv.x * horizontal + uv.y * vertical));
    vec3 col = color(r);
    gl_FragColor = vec4(col, 1.0);
}
