/**
 * b1_ch6.glsl
 * Ray Tracing in One Weekend
 * Chapter 6 - Surface Normals and Multiple Objects
 */

 #include "./common.glsl"

bool hit_world(Ray r, float tmin, float tmax, out HitRecord rec)
{
    bool hit = false;
    rec.t = tmax;

    if(hit_sphere(
        createSphere(vec3(0.0, 0.0, -1.0), 0.5),
        r,
        tmin,
        rec.t,
        rec))
    {
        hit = true;
    }

    if(hit_sphere(
        createSphere(vec3(0.0, -100.5, -1.0), 100.0),
        r,
        tmin,
        rec.t,
        rec))
    {
        hit = true;
    }

    return hit;
}

vec3 color(Ray r)
{
    HitRecord rec;
    if(hit_world(r, 0.0, 1000.0, rec))
    {
        vec3 n = rec.normal;
        return (n + 1.0) * 0.5;
    }
    float t = 0.5 * (r.d.y + 1.0);
    return (1.0 - t) * vec3(1.0) + t * vec3(0.5, 0.7, 1.0);
}

void main()
{
    Camera cam = createCamera(
        vec3(0.0, 0.0, 1.0),    // camera position
        vec3(0.0, 0.0, -1.0),   // look at
        vec3(0.0, 1.0, 0.0),    // world up vector
        90.0,
        iResolution.x / iResolution.y);
    vec2 uv = gl_FragCoord.xy / iResolution.xy;
    vec3 col = color(getRay(cam, uv));
    gl_FragColor = vec4(col, 1.0);
}
