/**
 * b1_ch2.glsl
 * Ray Tracing in One Weekend
 * Chapter 2 - Output an Image
 */

void main()
{
    vec2 rg = gl_FragCoord.xy / iResolution.xy;
    float b = 0.2;
    gl_FragColor = vec4(rg, b, 1.0);
}
